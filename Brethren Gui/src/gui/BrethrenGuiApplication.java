package gui;

import input.KeyHandler;

import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;

import client.ClientManager;
import player.PlayerConstants;


public class BrethrenGuiApplication{
    private ClientManager clientManager;
    private KeyHandler keyHandler;
    private JFrame frame;
    private BrethrenCanvas canvas;
    
    public BrethrenGuiApplication(ClientManager clientManager){
        this.clientManager = clientManager;
        this.keyHandler = new KeyHandler();
        
        this.canvas = new BrethrenCanvas(
        		PlayerConstants.boardHeightPixels,
        		PlayerConstants.boardWidthPixels,
        		this.keyHandler,
        		this.clientManager);
        setupFrameComponents();
    }
    
    public void setupFrameComponents(){
    	frame = new JFrame("Brethren Development");
    	Container cont;
    	cont = frame.getContentPane();
    	
    	cont.add(canvas);
    	frame.pack();
    	frame.setResizable(false);
    	frame.setLocationRelativeTo(null);
    	frame.toFront();
    	frame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				System.exit(0);
			}
		});
    }

	public void start() {
		frame.setVisible(true);
		startPainting();
	}
	
	public void startPainting(){
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate( new Runnable() {
            @Override
            public void run() {
				canvas.repaint();
			}
		}, 0, 30, TimeUnit.MILLISECONDS);
	}
	
	public KeyHandler getKeyHandler(){
		return this.keyHandler;
	}

}
