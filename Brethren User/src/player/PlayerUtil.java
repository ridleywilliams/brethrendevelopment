package player;

import java.nio.charset.StandardCharsets;

import message.Message;
import config.MessagingConfig;

public class PlayerUtil {
	public static String[] delimitBytes(Message m){
		String bytesAsString = new String(m.getMessageBody(), StandardCharsets.UTF_8);
		return bytesAsString.split(MessagingConfig.DELIMITER);
	}
}
