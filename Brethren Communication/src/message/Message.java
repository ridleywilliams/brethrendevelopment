package message;

import host.Host;

import java.net.SocketAddress;

public class Message {
	private MessageType type;
	private byte[] messageBody;
	private Host host;
	
	public Message(byte[] messageBody, Host host){
		this.messageBody = messageBody;
		this.type = calculateType(messageBody[0]);
		this.host = host;
	}
	
	private static MessageType calculateType(byte b) {
		return MessageType.valueOf(b);
	}

	public MessageType getType(){
		return this.type;
	}
	
	public void setType(MessageType type){
	    this.messageBody[0] = type.toByte();
	}
	
	public byte[] getMessageBody(){
		return this.messageBody;
	}
	
	public Host getHost(){
		return this.host;
	}
	
}
