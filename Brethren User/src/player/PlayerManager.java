package player;

import java.awt.Graphics;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import message.Message;

public class PlayerManager {
	private Map<String, Player> playerMap;
	public PlayerManager(){
		playerMap = new HashMap<String, Player>();
	}
	
	public void addNewPlayer(Message m){
		Player p = new Player(m);
		playerMap.put(p.getUsername(), p);
	}
	
	public void movePlayer(Message m){
		String[] tokens = PlayerUtil.delimitBytes(m);
		for(int i=2; i<tokens.length; i++){
		    playerMap.get(tokens[1]).move(tokens[i]);
		}
	}
	
	public Collection<Player> getPlayers(){
	    return playerMap.values();
	}

    public void addPlayer(Player player) {
        playerMap.put(player.getUsername(), player);   
    }

    public int getSize() {
        return playerMap.values().size();
    }
    
    public void paint(Graphics g){
    	for(Player p : this.getPlayers())
    		p.paint(g);
    }
}
