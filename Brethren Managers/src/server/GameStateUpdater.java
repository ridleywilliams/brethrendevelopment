package server;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import message.Message;
import message.MessageBuilder;
import game_state.GameState;
import game_state.GameStateSerializer;
import host.Host;


public class GameStateUpdater {
    private ServerManager serverManager;
    
    public GameStateUpdater(ServerManager serverManager){
        this.serverManager = serverManager;
    }
    
    public void start(){
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate( new Runnable() {
            @Override
            public void run() {
                //System.out.println(serverManager.getGameState().getPlayerManager().getSize()); //DEBUG
//                byte[] gameStateMessageBody = MessageBuilder.gameStateUpdateMessageBody(serverManager.getGameState());
                for(Host h: serverManager.getConnectedClients()){
//                    Message m = new Message(gameStateMessageBody, h);
                	Message m = GameStateSerializer.serialize(serverManager.getGameState(), h);
                    serverManager.addMessageToSendQueue(m);
                }   
            }       
        }, 0, 30, TimeUnit.MILLISECONDS);
    }
   
}
