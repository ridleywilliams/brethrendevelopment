package server;

import game_state.GameState;
import host.Host;

import java.net.DatagramSocket;
import java.util.HashSet;
import java.util.Set;

import player.PlayerManager;
import message.Message;
import message.ReceivingMessagingQueueOperator;
import message.SendingMessagingQueueOperator;
import message_processing.ClientMessageOperator;
import message_processing.ServerMessageOperator;


public class ServerManager implements Runnable{
    private ServerMessageOperator messageOperator;
    private ReceivingMessagingQueueOperator receievingMessagingQueue;
    private SendingMessagingQueueOperator sendingMessagingQueueOperator;
    private GameStateUpdater gameStateUpdater;
    private DatagramSocket socket;
    private Host host;
    private GameState gameState;
    public Set<Host> connectedClients;
    
    public ServerManager(DatagramSocket socket){
        this.socket = socket;
        this.host = new Host(socket.getInetAddress(),socket.getLocalPort());
        gameState = new GameState();
        System.out.println("host: "+host.getAddress()+":"+host.getPortNumber()); //TODO DEBUG; REMOVE LATER
        connectedClients = new HashSet<Host>();
        gameStateUpdater = new GameStateUpdater(this);
    }
    
    @Override
    public void run() {
        this.messageOperator = new ServerMessageOperator(this);
        this.receievingMessagingQueue = new ReceivingMessagingQueueOperator(socket, messageOperator);
        this.sendingMessagingQueueOperator = new SendingMessagingQueueOperator(socket);
        receievingMessagingQueue.start();
        sendingMessagingQueueOperator.start();
        gameStateUpdater.start();       
    }
    
    public PlayerManager getPlayerManager(){
        return gameState.getPlayerManager();
    }
    
    public void addMessageToSendQueue(Message m){
        this.sendingMessagingQueueOperator.addMessageToQueue(m);
    }
    
    public void connectClient(Host client){
        connectedClients.add(client);
    }
    
    public Set<Host> getConnectedClients(){
        return connectedClients;
    }
    
    public GameState getGameState(){
        return gameState;
    }
}
