package message_processing;

import game_state.GameState;
import game_state.GameStateSerializer;
import player.Player;
import client.ClientManager;
import message.Message;
import message.MessageUtils;

public class ClientMessageOperator implements MessageOperator{
    private ClientManager clientManager;
    
    public ClientMessageOperator(ClientManager clientManager){
        this.clientManager = clientManager;
    }
    
	@Override
	public void operateOnMessage(Message m) {
	    switch (m.getType()){
        case NEW_PLAYER_REC: 
            newPlayerReceived(m);
            break;
        case MOVEMENT_REC:
            movementReceived(m);
            break;
        case CONNECTION_REC:
            connectionReceived(m);
            break;
        case GAME_STATE_UPDATE:
            gameStateUpdate(m);
        default:
            break;
        }
	}
	
    private void newPlayerReceived(Message m) {
        clientManager.setPlayer(new Player(m));
    }

    private void movementReceived(Message m) {
        clientManager.getPlayer().move(m);
    }

    private void connectionReceived(Message m){
        clientManager.getConnectionSemaphore().release();
        clientManager.setConnected();

    }
    
    private void gameStateUpdate(Message m) {
        clientManager.setGameState(GameStateSerializer.deserialize(m));
    }
}
