package player;

public class PlayerConstants {
	public static final int boardHeight = 20;
	public static final int boardWidth = 20;
	public static final int tileSizePixels = 32;
	
	public static final int boardHeightPixels = boardHeight * tileSizePixels;
	public static final int boardWidthPixels = boardWidth * tileSizePixels;
}
