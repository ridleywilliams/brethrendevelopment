package host;

import java.net.InetAddress;

public class Host {
	private int portNumber;
	private InetAddress address;
	
	public Host(InetAddress address, int portNumber){
		this.portNumber = portNumber;
		this.address = address;
	}
	
	public int getPortNumber(){
		return this.portNumber;
	}
	
	public InetAddress getAddress(){
		return this.address;
	}
}
