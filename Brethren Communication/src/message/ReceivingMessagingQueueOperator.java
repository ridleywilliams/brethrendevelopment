package message;

import host.Host;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

import config.MessagingConfig;
import message_processing.MessageOperator;

public class ReceivingMessagingQueueOperator {
	private ConcurrentLinkedQueue<Message> messageQueue;
	private Semaphore elemInQueueSemaphore;
	private DatagramSocket socket;
	private MessageOperator messageOperator;
	
	public ReceivingMessagingQueueOperator(DatagramSocket socket, MessageOperator messageOperator){
		this.socket = socket;
		this.messageOperator = messageOperator;
		elemInQueueSemaphore = new Semaphore(0);
		messageQueue = new ConcurrentLinkedQueue<Message>();
	}
	
	public void start(){
	    startReceivingMessages();
	    startProcessingMessages();
	}
	
	private void startProcessingMessages() {
		Thread processingMessagesThread = new Thread(){
			public void run(){
				while(true){
					try {
						elemInQueueSemaphore.acquire();
						Message m = messageQueue.poll();
//						System.out.println("processing message: "+new String(m.getMessageBody(), StandardCharsets.UTF_8).trim()); //TODO DEBUG; REMOVE LATER
						messageOperator.operateOnMessage(m);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
				}
			}
		};
		processingMessagesThread.start();
		
	}

	private void startReceivingMessages(){
		Thread receivingMessagesThread = new Thread(){
			public void run(){
				byte[] buf = new byte[MessagingConfig.MaxUDPPacketSize];
		        DatagramPacket packet = new DatagramPacket(buf ,MessagingConfig.MaxUDPPacketSize);
		        while(true){
			        try {
			            socket.receive(packet);
			            Host origin = new Host(packet.getAddress(), packet.getPort());
//			            System.out.println("received message: "+new String(packet.getData(), StandardCharsets.UTF_8).trim()+" from "+origin.getAddress()+":"+origin.getPortNumber()); //TODO DEBUG; REMOVE LATER
			            messageQueue.add(new Message(packet.getData(), origin));
			            elemInQueueSemaphore.release();
			        } catch (IOException e){
			            e.printStackTrace();
			        }   
		        }
			}
		};
		receivingMessagesThread.start();
	}
}
