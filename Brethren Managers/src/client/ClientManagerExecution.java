package client;

import java.net.DatagramSocket;
import java.net.SocketException;

import message.MessageUtils;


public class ClientManagerExecution {
    public static void main (String args[]){
        try {
            DatagramSocket socket = new DatagramSocket();
            ClientManager clientManager = new ClientManager(socket);
            new Thread(clientManager).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }
        
    }
}
