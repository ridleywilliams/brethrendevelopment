package message_processing;

import java.nio.charset.StandardCharsets;

import server.ServerManager;
import message.Message;
import message.MessageType;

public class ServerMessageOperator implements MessageOperator{
    private ServerManager serverManager;
    
    public ServerMessageOperator(ServerManager serverManager){
        this.serverManager = serverManager;
    }
    
    @Override
    public void operateOnMessage(Message m) {
    	if(m.getType() == null){
    		System.out.println(new String(m.getMessageBody(), StandardCharsets.UTF_8).trim()+" has null type");
    		return;
    	}
        switch (m.getType()){
        case NEW_PLAYER_SEND: 
            newPlayerSent(m);
            break;
        case MOVEMENT_SEND:
            movementSent(m);
            break;
        case CONNECTION_SEND:
            connectionSent(m);
            break;
        default:
            break;
        }
    }

    private void newPlayerSent(Message m) {
        System.out.print("NEW PLAYER Message");
        serverManager.getPlayerManager().addNewPlayer(m);
        m.setType(MessageType.NEW_PLAYER_REC);
        serverManager.addMessageToSendQueue(m);
    }

    private void movementSent(Message m) {
        serverManager.getPlayerManager().movePlayer(m);
        m.setType(MessageType.MOVEMENT_REC);
        serverManager.addMessageToSendQueue(m);
    }
    
    private void connectionSent(Message m) {
        m.setType(MessageType.CONNECTION_REC);
        serverManager.addMessageToSendQueue(m);
        serverManager.connectClient(m.getHost());
    }
}
