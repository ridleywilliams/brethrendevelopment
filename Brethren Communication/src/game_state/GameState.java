package game_state;

import java.awt.Graphics;
import java.io.Serializable;

import player.Player;
import player.PlayerManager;


public class GameState implements Serializable{
    private PlayerManager playerManager;
    
    public GameState(PlayerManager playerManager){
        this.playerManager = playerManager;
    }
    
    public GameState(){
        this.playerManager = new PlayerManager();
    }
    
    public GameState(Player player) {
        this.playerManager = new PlayerManager();
        playerManager.addPlayer(player);
    }
    
    public void paint(Graphics g){
    	this.playerManager.paint(g);
    }

    public PlayerManager getPlayerManager(){
        return this.playerManager;
    }
}
