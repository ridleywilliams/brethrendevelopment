package message;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;

import player.Player;
import player.PlayerManager;
import config.MessagingConfig;
import game_state.GameState;
import host.Host;


public class MessageUtils {
    public static Host getHostFromConnectionMessage(Message m) { 
        try {
            String[] connectionMessageArgs = delimitBytes(m.getMessageBody(), MessagingConfig.DELIMITER);
            InetAddress addr;
            addr = InetAddress.getByName(connectionMessageArgs[1]);
            int portNumber = Integer.valueOf(connectionMessageArgs[2]);
            return new Host(addr, portNumber);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    //For some reason this isn't working.
    private static String[] delimitBytes(byte[] bytes, String delimiter){
        String bytesAsString = new String(bytes, StandardCharsets.UTF_8);
        return bytesAsString.split(delimiter);
    }
    
    /**
     * Debug method (kinda?) for returning InetAddress of localhost
     * @return InetAddress of host
     */
    public static InetAddress getHostAddress(){
		try{
			return InetAddress.getLocalHost();
		}catch(UnknownHostException e){
			return null;
		}
	}
    
    public static GameState getGameStateFromMessage(Message m){
        PlayerManager playerManager = new PlayerManager();
        String[] playerStrings = delimitBytes(m.getMessageBody(), MessagingConfig.PLAYER_DELIMITER);
        for(int i=1; i<playerStrings.length; i++){ //Start at 1 because 0th element will be the message type.
            playerManager.addPlayer(new Player(playerStrings[i]));
        }
        return new GameState(playerManager);
    }
}
