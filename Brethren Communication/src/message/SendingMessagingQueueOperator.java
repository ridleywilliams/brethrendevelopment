package message;

import host.Host;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class SendingMessagingQueueOperator {
	private ConcurrentLinkedQueue<Message> messageQueue;
	private Semaphore elemInQueueSemaphore;
	private Host receiver;
	private DatagramSocket socket;
	
	public SendingMessagingQueueOperator(DatagramSocket socket){
		this.socket = socket;
		elemInQueueSemaphore = new Semaphore(0);
		messageQueue = new ConcurrentLinkedQueue<Message>();
	}
	
	public void start(){
	    startSendingMessages();
	}
	
	public void addMessageToQueue(Message message){
		messageQueue.add(message);
		elemInQueueSemaphore.release();	
	}

	private void startSendingMessages(){
		Thread sendMessagesThread = new Thread(){
			public void run(){
				while(true){
					try {
						elemInQueueSemaphore.acquire();
						Message m = messageQueue.poll();
						receiver = m.getHost();
                        DatagramPacket packet = new DatagramPacket(m.getMessageBody(), m.getMessageBody().length, receiver.getAddress(), receiver.getPortNumber());
                        socket.send(packet);
//                        System.out.println("sent message: "+new String(m.getMessageBody(), StandardCharsets.UTF_8).trim()+" to "+receiver.getAddress()+":"+receiver.getPortNumber()); //TODO DEBUG; REMOVE LATER
					} catch (InterruptedException | IOException e) {
					    e.printStackTrace();
					}
					
				}
			}
		};
		sendMessagesThread.start();
	}
}
