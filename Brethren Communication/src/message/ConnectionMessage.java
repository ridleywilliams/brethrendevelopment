package message;

import host.Host;


public class ConnectionMessage extends Message{

    public ConnectionMessage(byte[] messageBody, Host host) {
        super(messageBody, host);
    }

}
