package message;

import java.nio.charset.StandardCharsets;

import player.Player;
import player.PlayerUtil;
import config.MessagingConfig;
import game_state.GameState;
import host.Host;


public class MessageBuilder {
    
    //TODO: change this method so that it takes in a player instead of these arguments
    public static Message sendNewPlayerMessage(String username, int xPos, int yPos, int xSpeed, int ySpeed, Host recipient){
        String messageBody = new String();
        messageBody += "@" + MessagingConfig.DELIMITER + 
                username + MessagingConfig.DELIMITER +
                xPos + MessagingConfig.DELIMITER + 
                yPos + MessagingConfig.DELIMITER +
                xSpeed + MessagingConfig.DELIMITER + 
                ySpeed + MessagingConfig.DELIMITER; 
        byte[] messageBytes = messageBody.getBytes();
        messageBytes[0] = MessageType.NEW_PLAYER_SEND.toByte();
        return new Message (messageBytes, recipient);
    }
    
    public static Message sendMovePlayerMessage(String username, String movementString, Host recipient){
        String messageBody = new String();
        messageBody += "@" + MessagingConfig.DELIMITER + 
                username + MessagingConfig.DELIMITER 
                + movementString + MessagingConfig.DELIMITER;
        byte[] messageBytes = messageBody.getBytes();
        messageBytes[0] = MessageType.MOVEMENT_SEND.toByte();
        return new Message(messageBytes, recipient);
    }
    
    public static Message sendConnectionRequestMessage(Host recipient){
        String messageBody = new String();
        messageBody += "@";
        byte[] messageBytes = messageBody.getBytes();
        messageBytes[0] = MessageType.CONNECTION_SEND.toByte();
        return new Message(messageBytes, recipient);
    }

    public static byte[] gameStateUpdateMessageBody(GameState gameState) {
        String messageBody = new String();
        messageBody += "@";
        for(Player p: gameState.getPlayerManager().getPlayers()){
            messageBody += MessagingConfig.PLAYER_DELIMITER + getPlayerString(p);
        }
        byte[] messageBytes = messageBody.getBytes();
        messageBytes[0] = MessageType.GAME_STATE_UPDATE.toByte();
        return messageBytes;
    }
    
    private static String getPlayerString(Player p){
        String playerString = new String();
                playerString += p.getUsername() + MessagingConfig.DELIMITER +
                p.getXPos() + MessagingConfig.DELIMITER + 
                p.getYPos() + MessagingConfig.DELIMITER +
                p.getXSpeed() + MessagingConfig.DELIMITER + 
                p.getXSpeed() + MessagingConfig.DELIMITER; 
       
        return playerString;              
    }
}
