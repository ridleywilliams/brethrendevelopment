package config;

import java.net.InetAddress;

public class MessagingConfig{
	public static final int MaxUDPPacketSize = 1024;
	public static final String DELIMITER = "`%`";
	public static final String PLAYER_DELIMITER = "Pp%pP";

}
