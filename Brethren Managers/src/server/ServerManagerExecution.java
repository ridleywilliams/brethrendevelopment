package server;

import java.net.DatagramSocket;
import java.net.SocketException;

import message.MessageUtils;
import client.ClientManager;
import client.ClientManagerConfig;


public class ServerManagerExecution {
        public static void main (String args[]){
            try {
                DatagramSocket socket = new DatagramSocket(ClientManagerConfig.destinationServerPort);
                ServerManager serverManager = new ServerManager(socket);
                new Thread(serverManager).start();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
}
