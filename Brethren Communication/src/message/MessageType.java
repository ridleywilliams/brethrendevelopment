package message;

import java.io.Serializable;

public enum MessageType implements Serializable {
	NEW_PLAYER_SEND (0), NEW_PLAYER_REC(1), MOVEMENT_SEND(2), MOVEMENT_REC(3), CONNECTION_SEND(4), CONNECTION_REC(5), GAME_STATE_UPDATE(6);
	private int value;
	MessageType(int n){
		value = n;
	}
	
	public int value(){
		return this.value;
	}
	
	public static MessageType valueOf(int n){
		for (MessageType m: MessageType.values()){
			if(m.value() == n){
				return m;
			}
		}
		return null;
	}
	
	public byte toByte(){
	    return (byte)this.value;
	}
}
