package game_state;

import host.Host;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import config.MessagingConfig;
import message.Message;
import message.MessageType;

public class GameStateSerializer {
	
	public static Message serialize(GameState gameState, Host host){
		Message m;
		try{
	        ByteArrayOutputStream byteOStream = new ByteArrayOutputStream();
	        byteOStream.write(MessageType.GAME_STATE_UPDATE.toByte());
	        byteOStream.write(MessagingConfig.DELIMITER.getBytes());
	        ObjectOutputStream objectOStream = new ObjectOutputStream(byteOStream);
	        objectOStream.writeObject(gameState);
	        m = new Message(byteOStream.toByteArray(), host);
		}catch(IOException e){
			return null;
		}
        return m;
    }

    public static GameState deserialize(Message m){
    	GameState gameState;
    	try{
	    	byte[] messageBody = m.getMessageBody();
	        ByteArrayInputStream byteIStream = new ByteArrayInputStream(messageBody);
	        ObjectInputStream objectIStream = new ObjectInputStream(byteIStream);
	        gameState = (GameState)objectIStream.readObject();
    	}catch(IOException|ClassNotFoundException e){
    		return null;
    	}
        return gameState;
    }
}
