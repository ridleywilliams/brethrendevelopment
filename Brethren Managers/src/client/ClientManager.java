package client;

import input.InputProcessor;

import java.awt.Graphics;
import java.net.DatagramSocket;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import host.Host;
import message.Message;
import message.MessageBuilder;
import message.MessageUtils;
import message.ReceivingMessagingQueueOperator;
import message.SendingMessagingQueueOperator;
import message_processing.ClientMessageOperator;
import player.Player;
import player.PlayerConstants;
import player.PlayerUtil;
import game_state.GameState;
import gui.BrethrenGuiApplication;


public class ClientManager implements Runnable {
    private BrethrenGuiApplication brethrenGuiApplication;
    private ClientMessageOperator messageOperator;
    private ReceivingMessagingQueueOperator receievingMessagingQueue;
    private SendingMessagingQueueOperator sendingMessagingQueueOperator;
    private InputProcessor inputProcessor;
    private Player player = new Player(); //DEBUG purposes.
    private GameState gameState;
    private DatagramSocket socket;
    private Host host;
    private boolean isConnected = false;
    private Semaphore connectionSemaphore = new Semaphore(0);
    
    public ClientManager(DatagramSocket socket){
        this.brethrenGuiApplication = new BrethrenGuiApplication(this);
        this.socket = socket;
        this.host = new Host(MessageUtils.getHostAddress(), ClientManagerConfig.destinationServerPort);
        System.out.println("host: "+host.getAddress()+":"+host.getPortNumber()); //TODO DEBUG; REMOVE LATER
    }

    @Override
    public void run() {
    	//TODO should these instantiations be in the constructor or nah? Ask young money.
        this.messageOperator = new ClientMessageOperator(this);
        this.receievingMessagingQueue = new ReceivingMessagingQueueOperator(socket, messageOperator);
        this.sendingMessagingQueueOperator = new SendingMessagingQueueOperator(socket);
        this.brethrenGuiApplication = new BrethrenGuiApplication(this);
        this.inputProcessor = new InputProcessor(this.brethrenGuiApplication.getKeyHandler(), this.sendingMessagingQueueOperator, this);
        
        receievingMessagingQueue.start();
        sendingMessagingQueueOperator.start();
        
        
        for(int i=0; i<ClientManagerConfig.NUM_CONNECTION_ATTEMPTS; i++){
            Thread t = new Thread(){
                @Override
                public void run() {
                    sendingMessagingQueueOperator.addMessageToQueue(MessageBuilder.sendConnectionRequestMessage(host));
                }        
            };
            t.start();
        }
        try {
            connectionSemaphore.tryAcquire(ClientManagerConfig.CONNECTION_WAIT_TIME, TimeUnit.MILLISECONDS);
            if(isConnected==false) return; //More error handling for when a connection is not made
        } catch (InterruptedException e) {
            e.printStackTrace();
            return;
        }
        while(this.player==null) //pretty beat. 
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        //TODO request new player and wait for response; should be cleaned up
        Message m = MessageBuilder.sendNewPlayerMessage(""+System.currentTimeMillis(), 2*PlayerConstants.tileSizePixels, 2*PlayerConstants.tileSizePixels, 3, 3, this.host);
        this.sendingMessagingQueueOperator.addMessageToQueue(m);
        
        
        
        //TODO this should only start once player appears/client connects to server
        this.brethrenGuiApplication.start();
        this.inputProcessor.start();
    }
    
    public Player getPlayer(){
        return this.player;
    }
    
    public Host getHost(){
    	return this.host;
    }
    
    public void addMessageToSendQueue(Message m){
        this.sendingMessagingQueueOperator.addMessageToQueue(m);
    }
    
    public void setPlayer(Player player) {
        this.player = player;
        this.gameState = new GameState(player);
    }
    
    public void paint(Graphics g){
    	/*
    	 * TODO
    	 * Paint everything in the GameState object.
    	 * Every visual entity should have a paint(Graphics) method
    	 * that will be called from here. Pass in 'g' parameter.
    	 * In my old programs, all visual entities subclassed and
    	 * abstract Entity class that had methods such as getX(),
    	 * getY(), and paint(Graphics)
    	 */
    	if(this.gameState!=null)
    		this.gameState.paint(g);
    }
    
    public Semaphore getConnectionSemaphore(){
        return connectionSemaphore;
    }
    
    public void setConnected(){
        isConnected=true;
    }
    
    public void setGameState(GameState gameState){
        this.gameState = gameState;
    }
    
}
