 package player;

import game_state.GameState;

import java.awt.Color;
import java.awt.Graphics;
import java.nio.charset.StandardCharsets;

import message.Message;
import config.MessagingConfig;

public class Player {
	private String username;
	private int xPos;
	private int yPos;
	private int xSpeed;
	private int ySpeed;
	 
	/**
	 * Default shit constructor, debug purposes
	 */
	public Player(){
		username = ""+System.currentTimeMillis();
		xPos = yPos = 2*PlayerConstants.tileSizePixels;
		xSpeed = ySpeed = 3;
	}
	
	public Player(Message m){
		String[] tokens = PlayerUtil.delimitBytes(m);
		username = tokens[1];
		xPos = Integer.valueOf(tokens[2]);
		yPos = Integer.valueOf(tokens[3]);
		xSpeed = Integer.valueOf(tokens[4]);
		ySpeed = Integer.valueOf(tokens[5]);
	}
	
	public Player(String playerString){
	    String[] tokens = playerString.split(MessagingConfig.DELIMITER);
	    username = tokens[0];
        xPos = Integer.valueOf(tokens[1]);
        yPos = Integer.valueOf(tokens[2]);
        xSpeed = Integer.valueOf(tokens[3]);
        ySpeed = Integer.valueOf(tokens[4]);
	}

	public String getUsername(){
		return username;
	}
	
	public int getXPos(){
		return xPos;
	}
	
	public int getYPos(){
		return yPos;
	}
	
	public int getXSpeed(){
	    return xSpeed;
	}
	
	public int getYSpeed(){
	    return ySpeed;
	}
	
	public void move(Message m){
	    String[] tokens = PlayerUtil.delimitBytes(m);
	    this.move(tokens[2]);
	}

	public void move(String movementString) {
		for (int i=0; i<movementString.length(); i++){
			switch(movementString.charAt(i)){
			case 'u':
				yPos -= ySpeed;
				if(yPos<0)yPos = 0;
				break;
			case 'd':
				yPos += ySpeed;
				if(yPos>(PlayerConstants.boardHeightPixels -1))yPos = PlayerConstants.boardHeightPixels -1;
				break;
			case 'l':
				xPos -= xSpeed;
				if(xPos<0)xPos = 0;
				break;
			case 'r':
				xPos += xSpeed;
				if(xPos>(PlayerConstants.boardWidthPixels -1))xPos = PlayerConstants.boardWidthPixels -1;
				break;
			}
		}
	}
	
	public void paint(Graphics g){
		g.setColor(Color.green);
		g.fillOval(
				this.getXPos()-(int)(.5*PlayerConstants.tileSizePixels),
				this.getYPos()-(int)(.5*PlayerConstants.tileSizePixels),
				PlayerConstants.tileSizePixels,
				PlayerConstants.tileSizePixels);
		g.setColor(Color.black);
		g.drawString(this.username, this.getXPos(), this.getYPos());
	}
}
