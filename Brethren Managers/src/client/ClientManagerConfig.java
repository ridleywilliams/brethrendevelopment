package client;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class ClientManagerConfig {
    public static final int destinationServerPort = 8080;
    public static InetAddress getDestinationServerAddress(){
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static final int NUM_CONNECTION_ATTEMPTS = 3;
    public static final int CONNECTION_WAIT_TIME = 15000;
}
