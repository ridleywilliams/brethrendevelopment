package input;

import java.awt.event.MouseEvent;
import java.util.concurrent.ConcurrentLinkedQueue;

import message.Message;

public class KeyHandler {
	private boolean[] keysPressed;
	private ConcurrentLinkedQueue<MouseEvent> mouseEventQueue;
	
	//TODO refactor class name and all references from "KeyHandler" to "InputHandler"
	
	public KeyHandler(){
		this.keysPressed = new boolean[1000];
		//TODO not sure if this is needed, all bools may be initialized as false already
		for(int i = 0; i < 1000; i++)
			this.keysPressed[i] = false;
		this.mouseEventQueue = new ConcurrentLinkedQueue<MouseEvent>();
	}
	
	public void setKeyPressed(int key, boolean pressed){
		this.keysPressed[key] = pressed;
	}
	
	public boolean isPressed(int key){
		return this.keysPressed[key];
	}
	
	public void addMouseEvent(MouseEvent e){
		this.mouseEventQueue.add(e);
	}
	
	public MouseEvent getNextMouseEvent(){
		return this.mouseEventQueue.poll();
	}
}
