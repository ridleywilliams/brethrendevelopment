package input;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import client.ClientManager;
import message.MessageBuilder;
import message.SendingMessagingQueueOperator;

public class InputProcessor {
	private KeyHandler keyHandler;
	private SendingMessagingQueueOperator sendingMessagingQueueOperator;
	private ClientManager clientManager;
	
	public InputProcessor(KeyHandler keyHandler, SendingMessagingQueueOperator sendingMessagingQueueOperator, ClientManager clientManager){
		this.keyHandler = keyHandler;
		this.sendingMessagingQueueOperator = sendingMessagingQueueOperator;
		this.clientManager = clientManager;
	}
	
	public void start(){
		startProcessingInput();
	}
	
	public void startProcessingInput(){
		int[] keysToProcess = {
				KeyEvent.VK_UP,
				KeyEvent.VK_DOWN,
				KeyEvent.VK_LEFT,
				KeyEvent.VK_RIGHT};
		ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate( new Runnable() {
            @Override
            public void run() {
            	//process key input
				for(int key : keysToProcess)
					if(keyHandler.isPressed(key))
						processKey(key);
				//process mouse input
				MouseEvent mouseEvent = null;
				while((mouseEvent = keyHandler.getNextMouseEvent()) != null){
					System.out.println("MOUSE EVENT @ ("+mouseEvent.getX()+", "+mouseEvent.getY()+")"); //DEBUG
					//TODO do shit
				}
			}
		}, 0, 30, TimeUnit.MILLISECONDS);
	}
	
	public void processKey(int key){
		//TODO multiple directions can be supported with one single string
		switch(key){
		case KeyEvent.VK_UP:
			sendingMessagingQueueOperator.addMessageToQueue(
					MessageBuilder.sendMovePlayerMessage(this.clientManager.getPlayer().getUsername(), "u",	this.clientManager.getHost()));
			break;
		case KeyEvent.VK_DOWN:
			sendingMessagingQueueOperator.addMessageToQueue(
					MessageBuilder.sendMovePlayerMessage(this.clientManager.getPlayer().getUsername(), "d",	this.clientManager.getHost()));
			break;
		case KeyEvent.VK_LEFT:
			sendingMessagingQueueOperator.addMessageToQueue(
					MessageBuilder.sendMovePlayerMessage(this.clientManager.getPlayer().getUsername(), "l",	this.clientManager.getHost()));
			break;
		case KeyEvent.VK_RIGHT:
			sendingMessagingQueueOperator.addMessageToQueue(
					MessageBuilder.sendMovePlayerMessage(this.clientManager.getPlayer().getUsername(), "r",	this.clientManager.getHost()));
			break;
		}
	}
}
