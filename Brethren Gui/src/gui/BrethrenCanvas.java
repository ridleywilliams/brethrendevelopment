package gui;

import input.KeyHandler;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import client.ClientManager;
import player.PlayerConstants;

@SuppressWarnings("serial")
public class BrethrenCanvas extends JPanel implements KeyListener, MouseListener {
	private int height, width;
	private KeyHandler keyHandler;
	private ClientManager clientManager;
	
	public BrethrenCanvas(int height, int width, KeyHandler keyHandler, ClientManager clientManager){
		this.height = height;
		this.width = width;
		this.keyHandler = keyHandler;
		this.clientManager = clientManager;
		this.addKeyListener(this);
	}
	
	public boolean isFocusable(){
		return true;
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(width, height);
	}
	
	public Dimension getMinimumSize(){
		return new Dimension(width, height);
	}
	
	public Dimension getMaximumSize(){
		return new Dimension(width, height);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		g.clearRect(0, 0, PlayerConstants.boardWidthPixels, PlayerConstants.boardHeightPixels);
		clientManager.paint(g);
		/*
		 * THE FOLLOWING IS DEBUG AND MUST BE DELETED
		 */
		for(int i = (int)'0'; i <= (int)'9'; i++)
			if(keyHandler.isPressed(i))
				g.drawString((char)i+" pressed", 
						1 * PlayerConstants.tileSizePixels, 
						(i-(int)'0'+1) * PlayerConstants.tileSizePixels);
		//TODO DELETE DEBUG
	}
	
	public int getWidth(){
		return width;
	}
	
	public int getHeight(){
		return height;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		keyHandler.setKeyPressed(e.getKeyCode(), true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		keyHandler.setKeyPressed(e.getKeyCode(), false);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

	@Override
	public void mouseClicked(MouseEvent e) {
		keyHandler.addMouseEvent(e);
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

}
